<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>



</head>
<body>



<?php

$database="localhost";
$username="root";
$password="";
$dbname="test";



try{
    $pdo = new PDO("mysql:host=$database;dbname=$dbname", $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e){
    die("Chyba. Problem s pripojenim do DB. " . $e->getMessage());
}

// Attempt select query execution
try{
    $sql = "SELECT * FROM users";
    $result = $pdo->query($sql);
    if($result->rowCount() > 0){
        echo "<table id=\"myTable\" class=\"table table-striped table-bordered\" style=\"width:100%\">";

    echo "<thead>";
        echo "<tr>";
        echo "<th><a href=\"#\" onclick=\"sortTable(0)\"'>id</a></th>";
        echo "<th><a href=\"#\"  onclick=\"sortTable(1)\">uživatel</a></th>";
        echo "<th><a href=\"#\"  onclick=\"sortTable(2)\">rok narození</a></th>";

        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        while($row = $result->fetch()){
            echo "<tr>";
            echo "<td>" . $row['id'] . "</td>";
            echo "<td>" . $row['uzivatel'] . "</td>";
            echo "<td>" . $row['rok_narozeni'] . "</td>";

            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";

        unset($result);
    } else{
        echo "Nebyl nalezen zadny zaznam v DB.";

       }} catch(PDOException $e){
    die("Chyba:  $sql. " . $e->getMessage());
}

// Close connection
unset($pdo);
?>



<script>
    function sortTable(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("myTable");
        switching = true;
        dir = "asc";
        while (switching) {

            switching = false;
            rows = table.getElementsByTagName("TR");

            for (i = 1; i < (rows.length - 1); i++) {

                shouldSwitch = false;

                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];

                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {

                        shouldSwitch= true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {

                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {

                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;

                switchcount ++;
            } else {

                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }
</script>